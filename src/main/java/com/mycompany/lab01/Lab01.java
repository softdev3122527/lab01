/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author kornn
 */
public class Lab01 {
    static String[] board;
    static String turn;
     
    static void board()
    {
        System.out.println("|...|...|...|");
        System.out.println("| " + board[0] + " | " + board[1] + " | " + board[2] + " |");
        System.out.println("|...........|");
        System.out.println("| " + board[3] + " | " + board[4] + " | " + board[5] + " |");
        System.out.println("|...........|");
        System.out.println("| " + board[6] + " | "+ board[7] + " | " + board[8] + " |");
        System.out.println("|...|...|...|");
    }
    static String ResCheck()
    {for (int a = 0; a < 8; a++) {
        String line = null;
        switch (a) {
        case 0 -> line = board[0] + board[1] + board[2];
        case 1 -> line = board[3] + board[4] + board[5];
            case 2 -> line = board[6] + board[7] + board[8];
        case 3 -> line = board[0] + board[3] + board[6];
        case 4 -> line = board[1] + board[4] + board[7];
        case 5 -> line = board[2] + board[5] + board[8];
        case 6 -> line = board[0] + board[4] + board[8];
            case 7 -> line = board[2] + board[4] + board[6];
        }
        if (line.equals("XXX")) {
            return "X";
        }
        else if (line.equals("OOO")) {
            return "O";
        }
    }
    for (int a = 0; a < 9; a++) {
            if (Arrays.asList(board).contains(
                    String.valueOf(a + 1))) {
                break;
            }
            else if (a == 8) {
                return "draw";
            }
        }
 
        System.out.println(
            turn + " : Enter the number position "
            + turn + " in:");
        return null;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        board = new String[9];
        turn = "X";
        String winner = null;
 
        for (int a = 0; a < 9; a++) {
            board[a] = String.valueOf(a + 1);
        }
        System.out.println(" Welcome to XO Game!!! ");
        board();
        
        System.out.println(
        "X turn first. Enter the number position :");
        
        while (winner == null) {
            int numInput;
            try {
                numInput = in.nextInt();
                if (!(numInput > 0 && numInput <= 9)) {
                    System.out.println("Invalid Number; Please re-enter the number position:");
                    continue;
                }
            }
            catch (InputMismatchException e) {
                System.out.println(
                   "Invalid Number; Please re-enter the number position:");
                continue;
            }
            if (board[numInput - 1].equals(
                    String.valueOf(numInput))) {
                board[numInput - 1] = turn;
                if (turn.equals("X")) {
                    turn = "O";
                }
                else {
                    turn = "X";
                }
                board();
                winner = ResCheck();
            }
            else {
                System.out.println("This number is not empty, Please re-enter the number position:");
            }
        }
        if (winner.equalsIgnoreCase("draw")) {
            System.out.println( "It's a draw!, See you next game if you want.");
        }
        else {
            System.out.println( winner + " "+ "is the WINNER!.");
        }
      in.close();
    }
}
